

import 'package:get/get.dart';
import 'package:get_simple/presentation/binding/main_binding.dart';
import 'package:get_simple/presentation/binding/splash_controller_binding.dart';
import 'package:get_simple/presentation/ui/login/code/code_screen.dart';
import 'package:get_simple/presentation/ui/login/login_screen.dart';
import 'package:get_simple/presentation/ui/main/nav_bar_bottom/bottom_nav_bar.dart';
import 'package:get_simple/presentation/ui/splash/splash_screen.dart';

class AppRoutes{
  static final String splash = '/splash';
  static final String login = '/login';
  static final String main = '/main';
  static final String code = '/code';
}

class AppPages{
  static final pages = [
    GetPage(name: AppRoutes.splash, page:()=> SplashScreen(), binding: SplashControllerBinding()),
    GetPage(name: AppRoutes.login, page:()=> LoginScreen() ),
    GetPage(name: AppRoutes.code, page:()=> CodeScreen() ),
    GetPage(name: AppRoutes.main, page:()=> BottomNavigationBarScreen(),
        binding: MainScreenBinding()),

  ];
}