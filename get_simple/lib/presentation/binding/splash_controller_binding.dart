

import 'package:get/get.dart';
import 'package:get_simple/presentation/ui/splash/splash_controller.dart';

class SplashControllerBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<SplashController>(() => SplashController());
  }

}