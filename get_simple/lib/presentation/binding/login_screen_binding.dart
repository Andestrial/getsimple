

import 'package:get/get.dart';
import 'package:get_simple/presentation/ui/login/login_screen.dart';

class LoginScreenBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<LoginScreen>(() => LoginScreen());
  }

}