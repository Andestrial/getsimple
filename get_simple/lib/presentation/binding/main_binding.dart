
import 'package:get/get.dart';
import 'package:get_simple/presentation/ui/main/nav_bar_bottom/bottom_navigation_controller.dart';
import 'package:get_simple/presentation/ui/splash/splash_controller.dart';

class MainScreenBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<BottomNavigationController>(() => BottomNavigationController());
  }

}