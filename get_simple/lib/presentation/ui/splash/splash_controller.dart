

import 'package:get/get.dart';
import 'package:get_simple/presentation/router/router_page.dart';

class SplashController extends GetxController{

  @override
  void onReady() async{
    await Future.delayed(1.seconds, (){
      navigateToLogin();
    });
    super.onReady();
  }
  
  void navigateToLogin(){
    Get.offAndToNamed(AppRoutes.login);
  }
}