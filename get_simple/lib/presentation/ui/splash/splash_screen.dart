

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/presentation/ui/splash/splash_controller.dart';

class SplashScreen extends StatelessWidget {
  final splashController = Get.find<SplashController>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
                SvgPicture.asset(ImagesPath.SPLASH_LOGO_PATH),
                Text("Get Simple", style: TextStyle(fontSize: 48),)
            ],
          ),
        ),
      ),
    );
  }
}
