import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/ui/register/register_screen.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class CodeScreen extends StatefulWidget {
  @override
  _CodeScreenState createState() => _CodeScreenState();
}

class _CodeScreenState extends State<CodeScreen> {
  @override
  void initState() {
    Future.delayed(1.seconds, () {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Code : 984432"),
      ));
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  flex: 2,
                  child: SvgPicture.asset(ImagesPath.SPLASH_LOGO_PATH)),
              Expanded(
                  child: Container(
                margin: EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    PinCodeTextField(
                      pinTheme: PinTheme(
                          activeFillColor: Colors.grey,
                          disabledColor: Colors.grey,
                          activeColor: Colors.grey,
                          inactiveColor: Colors.grey),
                      length: 6,
                      appContext: context,
                      onChanged: (value) {},
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    RaisedButton(
                      onPressed: () {
                        Get.to(RegisterScreen());
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: Text('Continue',
                            style: TextStyle(color: Colors.white)),
                      ),
                      color: purpleColor,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(
                              color: purpleColor,
                              width: 1,
                              style: BorderStyle.solid),
                          borderRadius: BorderRadius.circular(10)),
                    )
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
