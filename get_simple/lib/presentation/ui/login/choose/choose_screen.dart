import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/router/router_page.dart';
import 'package:get_simple/presentation/ui/main/nav_bar_bottom/bottom_nav_bar.dart';

class ChooseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(child: SvgPicture.asset(ImagesPath.USER_SVG_PATH)),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      RaisedButton(
                        elevation: 0,
                        onPressed: () {
                          buildDialog(middleText: "student", onConfirm: (){
                            Get.offAll(BottomNavigationBarScreen(typeUser: 0,));
                          }, );
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 16),
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: Text('Student',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 18)),
                        ),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: purpleColor,
                                width: 2,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      SizedBox(
                        height: 70,
                      ),
                      RaisedButton(
                        elevation: 0,
                        onPressed: () {
                          buildDialog(middleText: "teacher", onConfirm: (){ Get.offAll(BottomNavigationBarScreen(typeUser: 1,));});
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 16),
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: Text('Teacher',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 18)),
                        ),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: purpleColor,
                                width: 2,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(10)),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  buildDialog({String middleText , @required Function onConfirm,}){
    return Get.defaultDialog(
      title: '',
      content: null,
      middleTextStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      middleText:
      "Are you sure you want to continue as a $middleText?",
      confirm: RaisedButton(
          elevation: 0,
          onPressed: onConfirm,
          color: Colors.white,
          child: Text("Continue")),
      cancel: RaisedButton(
          elevation: 0,
          onPressed: (){
            Get.back();
          },
          color: Colors.white,
          child: Text("Cancel")),
    );
  }
}
