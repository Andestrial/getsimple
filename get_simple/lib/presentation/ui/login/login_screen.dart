import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/ui/widgets/helpers_widgets.dart';

import 'code/code_screen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  flex: 2,
                  child: SvgPicture.asset(ImagesPath.SPLASH_LOGO_PATH)),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      appTextField(
                          hint: "Enter the phone number",
                          label: "Phone number"),
                      SizedBox(
                        height: 50,
                      ),
                      RaisedButton(
                        onPressed: () {
                          Get.to(CodeScreen());
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 16),
                          alignment: Alignment.center,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width * 0.9,
                          child: Text('Continue',
                              style: TextStyle(color: Colors.white)),
                        ),
                        color: purpleColor,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: purpleColor,
                                width: 1,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(10)),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
