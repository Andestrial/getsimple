import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/ui/login/choose/choose_screen.dart';
import 'package:get_simple/presentation/ui/main/nav_bar_bottom/bottom_navigation_controller.dart';
import 'package:get_simple/presentation/ui/widgets/helpers_widgets.dart';

class RegisterScreen extends StatelessWidget {
  final controller = Get.put(BottomNavigationController());

  final nameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    appTextField(
                        editingController: nameController,
                        hint: "Enter first name",
                        label: "Firs name",
                        disablePrefix: true),
                    appTextField(
                        editingController: lastNameController,
                        hint: "Enter last name",
                        label: "Last name",
                        disablePrefix: true),
                    appTextField(
                        editingController: emailController,
                        hint: "Enter email",
                        label: "Email",
                        disablePrefix: true),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(bottom: 100),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      RaisedButton(
                        onPressed: () {
                          controller.userEmail = emailController.text;
                          controller.userLastName = lastNameController.text;
                          controller.userName = nameController.text;

                          Get.to(ChooseScreen());
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 16),
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: Text('Continue',
                              style: TextStyle(color: Colors.white)),
                        ),
                        color: purpleColor,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: purpleColor,
                                width: 1,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
