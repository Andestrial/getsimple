import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/presentation/ui/main/course_detail/course_detail.dart';

class MyCoursesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
          itemBuilder: (context, index) => ListTile(
                onTap: () {
                  Get.to(CourseDetail(
                    courseTitle: "Example course",
                  ));
                },
                leading: SvgPicture.asset(
                  ImagesPath.SPLASH_LOGO_PATH,
                  width: 80,
                  height: 80,
                ),
                title: Text("Example course"),
                subtitle: Text("Teacher Name"),
              ),
          itemCount: 20,
          separatorBuilder: (BuildContext context, int index) => Divider(
                height: 1,
                thickness: 1,
              )),
    );
  }
}
