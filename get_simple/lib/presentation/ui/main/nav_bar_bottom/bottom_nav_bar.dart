import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/ui/main/all_courses/all_courses_screen.dart';
import 'package:get_simple/presentation/ui/main/my_courses/my_courses_screen.dart';
import 'package:get_simple/presentation/ui/main/nav_bar_bottom/bottom_navigation_controller.dart';
import 'package:get_simple/presentation/ui/main/notifications/notifications_screen.dart';
import 'package:get_simple/presentation/ui/main/profile/profile_screen.dart';

class BottomNavigationBarScreen extends StatelessWidget {
  BottomNavigationBarScreen({this.typeUser});

  final int typeUser;

  final bottomNavigationController =
      Get.find<BottomNavigationController>();
  List<Widget> _screens = [
    MyCoursesScreen(),
    NotificationsScreen(),
    AllCoursesScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    bottomNavigationController.typeUser = typeUser;
    return SafeArea(
      child: Scaffold(
          body: Obx(
              () => _screens.elementAt(bottomNavigationController.count.value)),
          bottomNavigationBar: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12),
                    topLeft: Radius.circular(12)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black38, spreadRadius: 0, blurRadius: 6),
                ],
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12.0),
                  topRight: Radius.circular(12.0),
                ),
                child: Obx(
                  () => BottomNavigationBar(
                    selectedItemColor: purpleColor,
                    unselectedItemColor: Colors.black,
                    showUnselectedLabels: true,
                    items: <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Icon(Icons.school_outlined),
                        label: 'My course',
                      ),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.notifications_active_outlined),
                          label: 'Notification'),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.public), label: 'All courses'),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.person), label: 'My profile')
                    ],
                    onTap: (index) {
                      bottomNavigationController.changeCount(index);
                    },
                    currentIndex: bottomNavigationController.count.value,
                  ),
                ),
              ))),
    );
  }
}
