import 'package:get/get.dart';

class BottomNavigationController extends GetxController {
  var count = 0.obs;

  var userName;
  var userLastName;
  var userEmail;
  var typeUser;

  void changeCount(int value) => count.value = value;
}
