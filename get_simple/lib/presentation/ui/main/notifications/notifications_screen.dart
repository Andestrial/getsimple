import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_simple/common/assets_path.dart';

enum NotificationsStates { withAccept, withoutAccept }

class NotificationsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemBuilder: (context, index) => Column(children: [
          ListTile(
            leading: SvgPicture.asset(
              ImagesPath.SPLASH_LOGO_PATH,
              width: 80,
              height: 80,
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 8),
            title: Text("Person Name created a course Example course. 12d"),
            // ),
            //  index % 5 == 0? Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //     children: [
            //       RaisedButton(
            //         elevation: 0,
            //         onPressed: () {
            //
            //         },
            //         child: Container(
            //           padding: EdgeInsets.symmetric(horizontal: 10),
            //           alignment: Alignment.center,
            //           child: Text('Continue',
            //               style: TextStyle(color: Colors.black)),
            //         ),
            //         color: Colors.white,
            //         shape: RoundedRectangleBorder(
            //             side: BorderSide(
            //                 color: Colors.black,
            //                 width: 1,
            //                 style: BorderStyle.solid),
            //             borderRadius: BorderRadius.circular(10)),
            //       ),
            //
            //       RaisedButton(
            //         elevation: 0,
            //         onPressed: () {
            //
            //         },
            //         child: Container(
            //           padding: EdgeInsets.symmetric(horizontal: 10),
            //           alignment: Alignment.center,
            //           child: Text('Continue',
            //               style: TextStyle(color: Colors.black)),
            //         ),
            //         color: Colors.white,
            //         shape: RoundedRectangleBorder(
            //             side: BorderSide(
            //                 color: Colors.black,
            //                 width: 1,
            //                 style: BorderStyle.solid),
            //             borderRadius: BorderRadius.circular(10)),
            //       )
            //     ],
            //   ): Offstage(),
          ),
        ]),
        itemCount: 20,
      ),
    );
  }
}
