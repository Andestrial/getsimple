import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/ui/main/course_detail/course_detail.dart';

class AllCoursesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.white,
          appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              flexibleSpace: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TabBar(
                    labelColor: Colors.black,
                    indicatorColor: purpleColor,
                    tabs: [
                      Tab(
                        text: 'Recommended ',
                      ),
                      Tab(text: 'All Courses')
                    ],
                  ),
                ],
              )),
          body: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Theme(
                  data: ThemeData(
                      primaryColor: Colors.grey, accentColor: Colors.grey),
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Search",
                      prefixIcon: Icon(Icons.search_outlined),
                    ),
                  ),
                ),
              ),
              Expanded(child: TabBarView(children: [buildList(), buildList()])),
            ],
          )),
    );
  }

  Widget buildList() {
    return GridView.builder(
      padding: const EdgeInsets.symmetric(vertical: 10),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 400,
          childAspectRatio: 0.8 ,
          crossAxisSpacing: 1,
          mainAxisSpacing: 20),
      itemBuilder: (context, index) => InkWell(
        onTap: (){
          Get.to(CourseDetail(courseTitle: "Example course",));
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          padding: const EdgeInsets.only(top:10, bottom: 10),
          alignment: Alignment.center,
          child: Column(
            children: [
              ListTile(
                title: Text("Example course"),
                subtitle: Text("Teacher Example Name"),
                trailing: SvgPicture.asset(
                  ImagesPath.SPLASH_LOGO_PATH,
                  width: 35,
                  height: 35,
                ),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Description is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been unknown printer took a type specimen  but also...",
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
           boxShadow: [
             BoxShadow(
               color: Colors.grey,
               blurRadius: 3,
               spreadRadius: 1,
             )
           ],
              color: Colors.white, borderRadius: BorderRadius.circular(15)),
        ),
      ),
      itemCount: 20,
    );
  }
}
