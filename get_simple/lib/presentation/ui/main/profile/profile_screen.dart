import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/common/colors.dart';
import 'package:get_simple/presentation/router/router_page.dart';
import 'package:get_simple/presentation/ui/main/nav_bar_bottom/bottom_navigation_controller.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  final controller = Get.find<BottomNavigationController>();

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Positioned.fill(
            top: 330,
            left: 20,
            right: 20,
            child: Align(
              alignment: Alignment.center,
              child: DefaultTabController(
                length: 2,
                child: Column(
                  children: [
                    Text(
                      "${widget.controller.userName + widget.controller.userLastName}",
                      style: TextStyle(fontSize: 38),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text("${widget.controller.userEmail}"),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "14 active courses",
                          style: TextStyle(fontSize: 20),
                        ),
                        Text("show")
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("14 active courses",
                            style: TextStyle(fontSize: 20)),
                        Text("show")
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("14 active courses",
                            style: TextStyle(fontSize: 20)),
                        Text("show")
                      ],
                    ),
                    TabBar(
                      labelColor: Colors.black,
                      indicatorColor: purpleColor,
                      tabs: [
                        Tab(
                          text: 'Followers ',
                        ),
                        Tab(text: 'Subscriptions')
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned.fill(
              top: 20,
              child: Align(
                  alignment: Alignment.topCenter,
                  child: SvgPicture.asset(
                    ImagesPath.USER_SVG_PATH,
                    width: 290,
                    height: 290,
                  ))),
          Positioned(
              top: 20,
              left: 30,
              child: Text(
                widget.controller.typeUser == 0 ? "STUDENT" : "TEACHER",
                style: TextStyle(color: Colors.black),
              )),
          Positioned(
            top: 6,
            right: 30,
            child: IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => SimpleDialog(
                          title: null,
                          titlePadding: EdgeInsets.zero,
                          children: [
                            SimpleDialogItem(
                              icon: Icons.edit,
                              color: purpleColor,
                              text: 'Edit profile',
                              onPressed: () {},
                            ),
                            SimpleDialogItem(
                              icon: Icons.notifications_off_outlined,
                              color: purpleColor,
                              text: 'Notifications',
                              onPressed: () {
                                Navigator.pop(context, 'user02@gmail.com');
                              },
                            ),
                            SimpleDialogItem(
                              icon: Icons.exit_to_app_sharp,
                              color: purpleColor,
                              text: 'Log out',
                              onPressed: () {
                                Future.delayed(0.3.seconds, () {
                                  Get.offAllNamed(AppRoutes.login);
                                });
                              },
                            ),
                          ],
                        ));
              },
            ),
          ),
        ],
      ),
    );
  }
}

class SimpleDialogItem extends StatelessWidget {
  const SimpleDialogItem(
      {Key key, this.icon, this.color, this.text, this.onPressed})
      : super(key: key);

  final IconData icon;
  final Color color;
  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SimpleDialogOption(
      onPressed: onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(icon, size: 24.0, color: color),
          Padding(
            padding: const EdgeInsetsDirectional.only(start: 16.0),
            child: Text(text),
          ),
        ],
      ),
    );
  }
}
