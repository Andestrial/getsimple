import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_simple/common/assets_path.dart';
import 'package:get_simple/common/colors.dart';

class CourseDetail extends StatefulWidget {
  const CourseDetail({Key key, this.courseTitle, this.description})
      : super(key: key);

  final String courseTitle;

  final String description;

  @override
  _CourseDetailState createState() => _CourseDetailState();
}

class _CourseDetailState extends State<CourseDetail> {
  bool scrollEnable = false;
  bool requestSend = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              title: Text(
                widget.courseTitle,
                style: TextStyle(color: Colors.black),
              ),
              centerTitle: true,
              leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                  )),
            ),
            body: scrollEnable
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ImagesPath.SPLASH_LOGO_PATH),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Text(
                          "Description is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been unknown printer took a type specimen  but also the leap  but also the leap but also the leap printer is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been unknown printer took a type specimen  but also the leap  but also the leap but also the leap printer is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been unknown printer took a type specimen  but also the leap  but also the leap but also the leap printer.",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        child: Text("Teacher : Teacher Name"),
                      ),
                      requestSend
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Request has been send",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 18)),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Icon(
                                    Icons.check,
                                    color: Colors.green,
                                  ),
                                ],
                              ),
                            )
                          : RaisedButton(
                              elevation: 0,
                              onPressed: () {
                                setState(() {
                                  scrollEnable = true;
                                  Future.delayed(0.3.seconds, () {
                                    setState(() {
                                      requestSend = true;
                                      scrollEnable = false;
                                    });
                                  });
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 16),
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: Text('Send Request',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 18)),
                              ),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: purpleColor,
                                      width: 1,
                                      style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(10)),
                            )
                    ],
                  )));
  }
}
