import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget appTextField({String hint, String label, bool disablePrefix = false, TextEditingController editingController}) {
  return Theme(
    data: ThemeData(
        primaryColor: Colors.deepPurpleAccent, primaryColorDark: Colors.red),
    child: TextField(
      controller: editingController,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
          prefixIcon: disablePrefix ? null : Icon(Icons.phone),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(12.0),
            ),
          ),
          filled: true,
          hintStyle: TextStyle(color: Colors.grey[800]),
          hintText: hint,
          labelText: label,
          fillColor: Colors.white70),
    ),
  );
}
